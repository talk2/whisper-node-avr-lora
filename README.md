# Overview #
This repository contains the documentation for the Wisen Talk² Whisper Node - AVR LoRa, an ultra-low power version of the Arduino with built-in LoRa Wireless communication and capable of running for years on a single AA battery.

For additional information about this product or about the Talk² project, please visit:

* Wisen Website and On-line Store at http://wisen.com.au
* Our official discussion Forum: http://talk2forum.wisen.com.au

![WhisperNode_LoRa_Antenna_AA_Logo_600x400.jpg](https://bitbucket.org/talk2/whisper-node-avr-lora/raw/master/Documentation/WhisperNode_LoRa_Antenna_AA_Logo_600x400.jpg)

|                 |                                        |
|----------------:|:---------------------------------------|
|**Product:**     |Talk² Whisper Node - AVR LoRa           |
|**Version:**     |1.1v - Official Release                 |
|**Release Date:**|5 November 2018                         |
|**Status:**      |Shipping                                |

*Use GIT Tags to navigate between different versions/releases.*

## Special Notes ##
Here some important notes regarding the Whisper Node:

* **This is a 3.3V board** - Although the MCU (Atmega328p) can run at 5V, there are components like the external SPI Flash and the RFM95 Radio Module that will not tolerate 5V. If you need to connect a 5V device to any of the pins, make sure the same pin is not shared with the external Flash neither the RFM95.
* **FTDI Voltage** - When powering the board via the FTDI pins, make sure the supplied voltage is between 3.4V and 6V as this will be using the LDO regulator (same as VIN).
* **Quick start** - If you are looking how to setup your Arduino IDE to work with this board, jump straight into the "Software" topic on this page to know how to install the Whisper Node Board, Talk² Library and other required libraries to run the examples.

## Table of Contents ##

[TOC]

# Hardware #

## Specifications ##

### Dimensions and Connections ###
* Board size: 65.65 x 26.75 x 1.6mm + connectors overhanging
* GPIOs: Two 17 Pin PCB Header to access all MCU Pins, Vin, Vbat, and the 3.3V Rail
* Standard 3x2 ISP Header
* 6 Pin FTDI Header
* Micro USB for External Power supply
* Pico-blade 1.25mm for Battery Connection
* u.FL/IPEX Connection for external Antenna
 
### Components ###
* MCU Atmel 8-bit AVR: ATMega328P-AU (32Kb Flash/2Kb RAM)
* 4Mbit SPI Flash: W25X40
* Sub-GHz RF Module: RFM95 (LoRa Semtech SX1276 IC)
* Step-up Switching Regulator: MCP16251
* LDO Regulator: MCP1700
* 16Mhz high precision Crystal
* 2x Feedback LEDs (Blue and Yellow)
* 2x Tactile User Buttons
* 1.5dBi (± 0.7 dBi) External u.FL PCB Antenna
 
### Optional Upgrade Kits ###
* CR2032 Battery Holder
* Real-Time Clock Kit: Maxim DS3231M
* High Input Voltage LDO 1117 Kit
* SMA Antenna Connector

## Board Layout ##
The layout below shows the position for the most important connectors and components.

### Pinout ###
The Talk² Whisper Node has many headers and connectors. All pins are positioned one a 2.54 grid and all PCB Headers are shipped with the board, making it compatible with a breadboard.

```
#!
             ┌─────  26.65mm  ─────┐

                   ╓───╖
                  ┌╢   ╟┐     26.65x64.65mm
             ╔════╡ CN1 ╞══════════╗        ┐
             ║┌─┐ └─────┘┌┐┌┐(H)┌─┐║        │ <--- Hole 2
             ║│■│     LD1└┘└┘LD2│■│║        │   20.0x61.5mm
             ║│o│               │o│║        │
             ║│o│               │o│║        │
             ║│o│               │o│║        │
             ║│o│<--P1     P2-->│o│║        │
             ║│o│               │o│║        │
             ║│o│               │o│║        │
             ║│o│  ┌───┐ ┌───┐  │o│║        │
             ║│o│  │BT1│ │BT2│  │o│║        │
             ║│o│  └───┘ └───┘  │o│║
             ║│o│               │o│║      64.65mm
             ║│o│               │o│║
             ║│o├───┐<--P3:ISP  │■│║        │
             ║│o│o o│         ┌─┤o│║        │
             ║│■│o o│P6:RTC-->│o│o│║        │
             ║│o├─┬─┴─────────┤o│o│║        │
             ║│o│ │o o o o o ■│o│o│║        │
             ║└─┘ └───────────┴─┴─┘║        │         Mounting Hole Diameter: 2.75mm
            ┌╨──╖     P4:FTDI      ║        │         Recommended Screw: Nylon M2.5
            │CN2║                  ║        │
            └╥──╜                  ║        │
Hole 1 -->  ┌╨╖  (H)           ┌───╢        │
7.8x10.3mm  └╥╜CN3    P5:VCC-->│o ■║        │
             ╚═════════════════╧═══╝        ┘
           0x0mm
```

Find below the pin-out and details about each one.

#### Extension Headers - P1 and P2 ####
The extension headers provide direct access to the MCU pins. Pins already in use by the Whisper Node board will have their function displayed next to each Pin number.

Note that some of the board functions can be disabled by removing SMD jumpers, look for more information about that on this document.

![WhisperNode-LoRa_1.0v_Pinout-Top.png](https://bitbucket.org/talk2/whisper-node-avr-lora/raw/master/Documentation/WhisperNode-LoRa_1.0v_Pinout-Top.png)

![WhisperNode-LoRa_1.0v_Pinout-Bottom.png](https://bitbucket.org/talk2/whisper-node-avr-lora/raw/master/Documentation/WhisperNode-LoRa_1.0v_Pinout-Bottom.png)

#### Antenna - CN1 ####
The 50Ω u.FL connector is attached to the RFM95 radio module. To prevent any damage to the RF module, make sure there's always an antenna connected when transmitting.

To improve the range a bigger or directional antenna might be used, ideally via a SMA Connector. Remember to keep the cable as short as possible to minimize signal lost.

#### Micro USB - CN2 and Picoblade - CN3 ####
The Talk² Whisper Node can be powered by Battery (VBAT) or an external Power Supply (VIN). The Micro USB connector is the standard way to connect a PSU to the board and the supplied voltage must be between 3.4V and 6V, a mobile phone USB A/C Charger or a USB power bank works pretty well.

For the battery connection, a power source between 0.9V and 3.4V can be attached to the micro connector Molex Picoblade. A 2xAA battery holder is the perfect example.

More detaild about the powering options can be found further on this document.
```
#!
          ║
          ║
         ┌╨───────╖*
         │       ─╢──── 1:VIN
         │       ─╢──── 2:NC
         │  CN2  ─╢──── 3:NC
         │       ─╢──── 4:NC
         │       ─╢──── 5:GND
         └╥───────╜
          ║
         ┌╨────╖*
         │    ─╢──── 1:VBAT
         │ CN3 ║
         │    ─╢──── 2:GND
         └╥────╜
          ╚═══════════

```

#### Programming Interfaces ISP - P3 and FTDI - P4 ####
Those are the headers to access ISP and FTDI connections to the MCU. It's important to note that although the MCU is 5V compatible the Radio Module RFM95 and the Flash SPI memory chip are not. For that reason never connect a 5V ISP programmer to the board or it might damage those components.
```
#!
           P3 - ICSP                   P4 - FTDI
     ┌───────────────────┐       ┌─────────────────────┐

          ┌──── 5:RESET             ┌─────────────┐*
          │ ┌── 3:SCK               │ o o o o o ■ │
          │ │ ┌ 1:MISO              └─────────────┘
          │ │ │                       │ │ │ │ │ │
        ┌───────┐*                    │ │ │ │ │ └ 1:GND
        │ o o o │                     │ │ │ │ └── 2:NC
        │ ■ o o │                     │ │ │ └──── 3:VIN
        └───────┘                     │ │ └────── 4:RXD
          │ │ │                       │ └──────── 5:TXD
          │ │ └ 2:VIN                 └────────── 6:DTR
          │ └── 4:MOSI
          └──── 6:GND

```
### Buttons and LEDs ###
The Talk² Whisper Node Buttons are not simple tactile switches connected to the MCU Pins, they actually include a small debounce circuit. If you're not familiar with the concept, a debounce circuit prevents a switch to oscillate between states at high frequency when pressed, smoothing its operation.

![WhisperNode_BT_Debounce_Circuit.png](https://bitbucket.org/talk2/whisper-node-avr-lora/raw/master/Documentation/WhisperNode_BT_Debounce_Circuit.png)

| Designator | Normal | Active | MCU Pin | Low to High | High to Low | PCB Jumper |
|------------|--------|--------|---------|-------------|-------------|------------|
| BT1        | LOW    | HIGH   | D4      | 0.5 ms      | 8.0 ms      |JP1 (on)    |
| BT2        | LOW    | HIGH   | D5      | 0.5 ms      | 8.0 ms      |JP3 (on)    |

*Note that both BT1 and BT2 are normal LOW, active HIGH. This has been designed like that to minimize any leakage but manly to be a bit more human friendly as an active LOW switch might be confusing.*

![WhisperNode_BT_Debounce_Low-High.png](https://bitbucket.org/talk2/whisper-node-avr-lora/raw/master/Documentation/WhisperNode_BT_Debounce_Low-High.png)
![WhisperNode_BT_Debounce_High-Low.png](https://bitbucket.org/talk2/whisper-node-avr-lora/raw/master/Documentation/WhisperNode_BT_Debounce_High-Low.png)

For reference, there's a interesting debounce online calculator: http://protological.com/debounce-calaculator/ which can assist you to calculate the resistor and capacitor values when designing such circuit.

For the LEDs, the board has two and they are simple 0603 LEDs with resistors at the Anode side, connected to a MCU pin via a SMD jumper:

| Designator | Color   | Resistor | MCU Pin | PCB Jumper |
|------------|---------|----------|---------|------------|
| LD1        | Blue    | 220R     | D6      |JP13 (on)   |
| LD2        | Yellow  | 220R     | D9      |JP14 (on)   |

*As a suggestion to preserve power, never lit a LED and perform a radio transmission at the same time. This will help to keep the total board consumption current as low as possible. Also, a 10ms to 25ms blink is normally more than enough to be noticed by most humans and will keep the energy consumption low.*

### Add-Ons RTC, LDO, Coin-Battery and SMA ###
To make use of all PCB real estate, the Talk² Whisper Node has a few blank Pads. With the correct components the board functionalities can be easily extended to add the following features like:

* I²C Real Time Clock with built-in Crystal and Temperature Sensor
* Extra 1117 LDO Regulator for Input Voltages over 12V
* CR2032 Coin-Cell battery Holder (can't be used together with 1117 LDO)
* SMA Connector

#### LDO - P5 ####
Even with Talk² Whisper Node being designed to run on batteries, it also can be powered by an external PSU limited to 5.5V. In case the supplied voltage is grater than 5.5V, a standard 1117 LDO Regulator can be soldered to the back of the board together with input and output 0805 ceramic capacitors.

The 1117 LDOs are not the most efficient regulators, with higher quiescent current and significant drop-out. The good thing about those devices is that the maximum input voltage is normally between 15V and 21V, depending on the manufacturer/model, being a great option when powering from a 12V source for example.

The 1117 LDO output is connected directly to the VIN, before the input Diode, the same way an external USB power supply would be connected. For that reason independent of the model used it must be output 5.5V and supply a minimum of 700mA.

**Attention:*** do not power the board through an 1117 LDO and an external power supply connected to the Micro USB at same time! But it's OK to power the board via the 1117 LDO and have a battery connected to VBAT working, as a power backup.

Suggested parts:

* Diodes AZ1117IH-5.0TRG1 - http://www.diodes.com/catalog/Single_LDOs_50/AZ1117_10276

##### Assemble #####

* LDO 1117 - SOT223: **U6**
* Ceramic Capacitor 10uF 35V - SMD 0805: **C17 and C18**
* PCB Block - 2.54mm: **P5**

#### RTC - P6 ####
The Talk² Whisper Node has a SOIC-8 footprints on the back to add an I²C Real Time Clock with built-in Crystal. The footprint matches the Maxim DS3231MZ+ (https://datasheets.maximintegrated.com/en/ds/DS3231M.pdf) and the Maxim DS3232MZ+ (https://datasheets.maximintegrated.com/en/ds/DS3232M.pdf) is also compatible.

If necessary special ports from the RTC chip can be accessed via P6 described below:

```
#!
       ┌─┐
       │o│──── RST
       │o│──── INT/SQW
       │o│──── 32kHz
       └─┘
```

The RTC chip has two power inputs, the VCC and VBAT. Normally when running by VCC the RTC consumes more power but it also has all features turned on. On the other hand, when powered exclusively by VBAT the chip will run in time-keeping mode only to save energy but still responding to I²C communication.

By default the RTC VCC pin is disconnected from the the 3V3_R1, only the VBAT is connected to the 3V3_R1 on the Talk² Whisper Node. This configuration will make the RTC Clock to run in low-power mode.

The board also makes possible to connect the VCC to the 3V3_R1 via the JP9. Alternatively the VCC can be connected to the MCU pin A1 using the JP10. In this last configuration the RTC VCC can be powered by turning the A1 Pin HIGH/LOW via software. Additionally the RTC INT can be connected to the MCU D3/INT1 Pin via a SMD jumper as well, so the RTC Alarms can be used to trigger interrupts on the MCU.

##### Assemble #####

* Maxim DS3231M - SOIC-8: **U7**
* Ceramic Capacitor 0.1uF 6V - SMD 0603: **C19 and C20**
* Film Resistor 100K - SMD0603: **R25 and R24**
* Film Resistor 4.7K - SMD0603: **R26 and R27**
* PCB Headers - 2.54mm: **P6** *(optional)*

#### Coin-Cell Holder - BAT1 ####
For ultra-low power applications, where size and weight matters, a small CR2032 coin-cell might be the best option. In this case a SMD battery retainer can be soldered to the board. The compatible parts are:

* BK-912 - http://www.batteryholders.com/part.php?pn=BK-912
* BAT-HLD-001 - https://www.linxtechnologies.com/en/products/connectors/battery-holders

The coin-cell battery terminals are attached directly to the step-up switching regulator, the same way the Picoblade - CN3 is. The coin-cell retainer shares the same PCB space as the 1117 LDO, so having both installed on the same board is not possible.

Before soldering the battery holder, make sure both round pads identified as "BAT1" are covered with a thin layer of solder. This is necessary to raise the contact pads, as well to prevent the battery terminal touching any other footprints.

Bear in mind that a CR2032 can supply 3V for most of it's life but the continuous current delivery is very limited. Simple coding strategies to minimize high peak current draw, like not blinking the LED at the same time the RFM95 module is being used, will significantly extend the battery's life.

Additionally, long data transmission might deplete the coin-cell battery very quickly or drop it's voltage to levels not supported by the step-up regulator.

#### SMA Connector - CN1 ####
If necessary a SMA Edge connector can be soldered directly to the board, making possible to attache a bigger antenna without a RF "pig-tail".

### Modifying Board Configuration ###
To give extra flexibility most of the MCU Pins on the Whisper Node are connected/disconnected through SMD Jumpers to free GPIOs according to the user's needs.

SMD jumpers are simple 0R resistor, designed to bridge the PCB traces. The same can be easily added or removed to change the physical configuration of the board. The table below describes all available SMD jumpers:

|Jumper|Default State|Purpose                   |Note                                                                                  |
|------|-------------|--------------------------|--------------------------------------------------------------------------------------|
|JP1   |Closed       |Button 1                  |Connects the Button 1 to the MCU D6 Pin                                               |
|JP2   |Closed       |SPI Flash                 |Connects the MCU Pin D8 to the SPI Clash Chip-select pin                              |
|JP3   |Closed       |Button 2                  |Connects the Button 2 to the MCU D5 Pin                                               |
|JP4   |Closed       |VIN Voltage Mosfet        |Controls the Mosfet (via A0) that allows the VIN voltage resistor to read voltage*    |
|JP5   |Closed       |Battery Voltage Mosfet    |Controls the Mosfet (via A0) that allows the battery voltage resistor to read voltage*|
|JP6   |Closed       |Vin Voltage               |Connects the VIN resistor divider to the MCU A7 Pin                                   |
|JP7   |Closed       |Battery Voltage           |Connects the battery resistor divider to the MCU A6 Pin                               |
|JP8   |Open         |Join VIN and VBAT         |Connects the VBAT and VIN as a single power supply                                    |
|JP9   |Open         |Powers the RTC from 3V3R1 |RTC VIN to 3.3V - Caution when using JP9 and JP10 at same time                        |
|JP10  |Open         |Powers the RTC from MCU A1|Can be used to power the RTC VIN from one of the MCU pin and enable all RTC features  |
|JP11  |Open         |RTC Int to MCU D3 (Int1)  |This can be used to connect the RTC Interrupt pin to the MCU D3 (Int1) to use alarms  |
|JP12  |Closed       |RFM95 Reset PIN to MCU D7 |Used to hardware reset the RFM95 module and bring it to a known state                 |
|JP13  |Closed       |LED1 (Blue)               |Connects the LED1 to the MCU D6                                                       |
|JP14  |Closed       |LED2 (Yellow)             |Connects the LED2 to the MCU D9                                                       |
|JP15  |Open         |RFM95 DIO1 to MCU A2      |Connects the RFM95 DIO1 to the MCU A2 - Required by the LoRaWAN LMIC                  |
|JP16  |Open         |RFM95 DIO2 to MCU A3      |Connects the RFM95 DIO2 to the MCU A3 - Required by the LoRaWAN LMIC                  |

**This exists to minimize current leakage thought the voltage divider when it's not being read.*

### Schematics ###
Most of the Talk² Whisper Node schematic is available at the "Documentation" folder on this repository - https://bitbucket.org/talk2/whisper-node-avr-lora/src/master/Documentation.

## Power ##
The Talk² Whisper Node can be powered by different sources or even combine multiple ones. The board has been specially designed to run on batteries, more specific from Alkaline cells. For that reason the board counts with two voltage regulators, a Microchip MCP16251 step-up switching regulator and a Microchip MCP1700 LDO.

### Battery: VBAT ###
The Battery input is directly connected to the MCP16251 step-up regulator. To be as efficient as possible there's no diode or protection on this line, do not reverse the polarity. The VBAT can be powered via the micro connector Molex Picoblade or from any PCB VBAT Pad - check the board pinout for details.

Although the board has been tested and able to run down to 0.75V, the recommended/start voltage must be between 0.9V and 3.3V.

*Note: Depending on the current requirement, the VBAT minimum voltage might need to be above 0.9V - see MCP16251 datasheet.

### Power Supply: VIN ###
The VIN is connected to the LDO MCP1700. The supplied voltage must be between 3.4V and 6V. The VIN can be powered via the Micro USB connector or from any PCB VIN Pad - check the board pinout for details.

### By-Passing the Regulators ###
An alternative way to power the board is supplying 3.3V directly to one of the 3V3R1 pins. Note by doing that you'll be by-passing any kind of built-in regulation, so make sure the power source is stable or it might cause permanent damage to the board.

### Power Backup ###
It's important to highlight that the VIN will take precedence over VBAT, if it's over 3.4V. In other words, once a power supply is connected to VIN, it'll disable the step-up regulator by pulling its enable pin low. In this configuration the battery consumption will be as low as 3uA.

In case the power supply is disconnected, or it stop working, the step-up regulator will be re-enabled and will start feeding 3.3V to the 3V3R1.

### Voltage Monitor ###
The Talk² Whisper Node counts with two resistor dividers configure as R Top: 562K and R Bot: 100K. This will result at 0.151V output for every 1V input. One resistor divider is attached to the VIN with the output to A7 and the other is attached to the VBAT through a P+N Mosfet, with the output to A6.

Both voltage dividers can bee used to monitor the input voltage levels via the MCU ADC and used to make the application behave accordingly to the available power sources. For the VBAT voltage divider, it's not always connect for power-saving. To read the VBAT voltage divider it's necessary first to bring the VBAT_Voltage_Mosfet pin high - check the board pinout for details.

Using the Internal MCU analog reference of 1.1V is possible to read up to 7.282V on any of the voltage dividers. The MCU counts with a 10bit resolution ADC, in other words, it can have a value from 0 to 1023. In a practical example, if the ADC is reading 211, it means the voltage before the voltage divider is around 1.5V:

```
Voltage = (MAX_Voltage * ADC_Reading) / 1024
Voltage = (7.282 * 211) / 1024
Voltage = 1.5
```
### Board Power Consumption ###
The board consumption was measured at the Battery Input (Step-up Regulator), but instead of a battery, an adjustable bench power supply was used to simulate multiple voltages.

**Test Modes**

* **All Sleeping:** MCU in Power-down sleeping mode, BOD and ADC disabled. SPI Flash and RFM95 Radio Module in Sleep modes;
* **All Sleeping + RTC:** Same as above plus optional RTC Chip soldered on the board and running in time-keeping mode;
* **MCU Running:** MCU in a "delay()" looping with all peripherals turned on. SPI Flash and RFM95 Radio Module in Sleep modes.

|Input Voltage|Mode                                  |Current|
|-------------|--------------------------------------|-------|
|3V           |All Sleeping                          |4.7µA  |
|3V           |All Sleeping + RTC                    |9.6µA  |
|3V           |MCU Running, Flash and Radio Sleeping |7.8mA  |
|2.5V         |All Sleeping                          |5.7µA  |
|2.5V         |All Sleeping + RTC                    |11.8µA |
|2.5V         |MCU Running, Flash and Radio Sleeping |9.8mA  |
|1.5V         |All Sleeping                          |8.6µA  |
|1.5V         |All Sleeping + RTC                    |15.5µA |
|1.5V         |MCU Running, Flash and Radio Sleeping |17.6mA |
|1V           |All Sleeping                          |10.1µA |
|1V           |All Sleeping + RTC                    |18.2µA |
|1V           |MCU Running, Flash and Radio Sleeping |27.8mA |
|0.8V         |All Sleeping                          |11.5µA |
|0.8V         |All Sleeping + RTC                    |25.6µA |
|0.8V         |MCU Running, Flash and Radio Sleeping |36.2mA |

*Note that the figures might have some variations (+- 2µA), which can be caused by factors like temperature, power supply stability, etc. Make sure the voltage is measured as close as possible to the board.*

### RFM95 Power Consumption ###
The Radio module is the component which consumes more energy, specially during transmission. To reduce power usage, a few methods can be implemented:

* Reduce TX power to minimum required;
* Reduce the message size, so less time is spent sending the message;
* Use highest TX speed possible;
* Use "sleep mode" as much as possible.

(TO BE PROVIDED)

### Regulator Efficiency ###
Below some test results from the Power Supply unit only. The tests were performed on a Talk² Whisper Node assembled only with the Power Supply components. The current was measured at the Input with in the following configuration modes: shutdown (the Step-up regulator disabled), no-load (the Step-up regulator enabled), 100 Kilo Ohms resistive load and 100Ohms resistive load.

#### Step-up ####
|Input Voltage|Output Voltage|Load*   |Input Current|Efficiency|
|-------------|--------------|--------|-------------|----------|
|3.4V         |n/a           |shutdown|n/a**        |TBD       |
|3.4V         |3.35V         |no-load |22µA         |TBD       |
|3.4V         |3.34V         |100KΩ   |34.8µA       |TBD       |
|3.4V         |3.31V         |100Ω    |32.8mA       |TBD       |
|3V           |n/a           |shutdown|2.0µA        |TBD       |
|3V           |3.27V         |no-load |4.7µA        |TBD       |
|3V           |3.27V         |100KΩ   |30.4µA       |TBD       |
|3V           |3.26V         |100Ω    |37.7mA       |TBD       |
|2.5V         |n/a           |shutdown|1.7µA        |TBD       |
|2.5V         |3.27V         |no-load |5.7µA        |TBD       |
|2.5V         |3.27V         |100KΩ   |38.4µA       |TBD       |
|2.5V         |3.24V         |100Ω    |44.9mA       |TBD       |
|2.0V         |n/a           |shutdown|1.3µA        |TBD       |
|2.0V         |3.27V         |no-load |6.8µA        |TBD       |
|2.0V         |3.27V         |100KΩ   |48.2µA       |TBD       |
|2.0V         |3.24V         |100Ω    |58.0mA       |TBD       |
|1.5V         |n/a           |shutdown|1.0µA        |TBD       |
|1.5V         |3.27V         |no-load |8.6µA        |TBD       |
|1.5V         |3.27V         |100KΩ   |59.4µA       |TBD       |
|1.5V         |3.24V         |100Ω    |82.8mA       |TBD       |
|1V           |n/a           |shutdown|0.7µA        |TBD       |
|1V           |3.27V         |no-load |10.1µA       |TBD       |
|1V           |3.27V         |100KΩ   |68.7µA       |TBD       |
|1V           |3.24V         |100Ω    |138.3mA      |TBD       |
|0.8V         |n/a           |shutdown|0.5µA        |TBD       |
|0.8V         |3.27V         |no-load |11.5µA       |TBD       |
|0.8V         |3.27V         |100KΩ   |79.8µA       |TBD       |
|0.8V         |3.24V         |100Ω    |165.7mA      |TBD       |

**The shutdown load represents the current consumption when the board is powered by the LDO regulator, which will place the Step-up in shutdown mode.*

***The "shutdown" mode is not valid in this scenario. In case the Step-up input voltage is higher than 3.3V, all current will be sourced from it, even in case there's a power supply connected to the LDO (VIN) Input.*

#### LDO ####
|Input Voltage|Output Voltage|Load    |Input Current|
|-------------|--------------|--------|-------------|
|5.0V         |3.29V         |no-load |8.9µA        |
|5.0V         |3.29V         |100KΩ   |42.4µA       |
|5.0V         |3.29V         |100Ω    |33.0mA       |
|4.0V         |3.29V         |no-load |8.3µA        |
|4.0V         |3.28V         |100KΩ   |41.7µA       |
|4.0V         |3.28V         |100Ω    |32.9mA       |
|3.5V         |3.28V         |no-load |7.9µA        |
|3.5V         |3.28V         |100KΩ   |41.3µA       |
|3.5V         |3.27V         |100Ω    |32.9mA       |
|3.4V         |n/a           |shutdown|1.9µA*       |

**Under 3.4V VIN the built-in voltage supervisor will disconnect the LDO and give priority for the Step-up regulator. The current measured here is actually to power the voltage supervisor IC.*

All tests above where performed at room temperature, around 25°C. The power source used was a Tenma 72-10480 and to measurement of the Step-up regulator was done through a µCurrent (https://www.eevblog.com/projects/ucurrent/). Also, all connections were kept as short as possible to reduce any voltage-drop across the wires.

# Software #
This section describes a bit about the standard/factory firmware as well the MCU Fuses and Bootloader configuration.

## Arduino IDE Boards ##
If you're using the Arduino IDE, you'll need a "Board Configuration" to upload code to the Whisper Node. This can be done by adding the following "Additional Boards Manager URL" to your Arduino IDE preferences: http://talk2arduino.wisen.com.au/master/package_talk2.wisen.com_index.json

For additional information, please refer to the https://bitbucket.org/talk2/arduino-ide-boards repository.

## Library ##
The Talk² Arduino Library can be installed using the "Library Manager" on the Arduino IDE. For all details and access to the library source code, please refer to the https://bitbucket.org/talk2/talk2-library repository.

## MCU Fuses ##
By default the Whisper Node AVR is shipped with the following fuses configured:

|Fuse     |Value |
|---------|------|
|High     |0xDA  |
|Low      |0xEF  |
|Extended |0x05  |

This sets the MCU to:

* use the external Crystal and clock divider = 1 (resulting in 16MHz)
* reserve 1024 pages for the bootloader (2048 bytes)
* set the BOD Level 1 (2.7V)

## Bootloader ##
The Talk² Whisper Nodes is pre-programmed with the Talk² Boot, a modified version of the Optiboot, capable of programming the MCU from an external SPI Flash memory. For all features and documentation, please refer to the Repo https://bitbucket.org/talk2/talk2boot.

**Default Sketch Upload Speed:** 115200kbp/s

## Factory Firmware ##
Apart from the bootloader, the Talk² Whisper Node is also programmed with a factory firmware (or sketch). This firmware is used to test each board during the final phase of the manufacturing process but also to provides some additional functionalities, like the support of "Over-the-Air" updates from factory.

The same firmware is present on the MCU Flash as well in a **write-protected** area of the external SPI Flash. More specifically from the memory address 0x0007000 to 0x0007FFFF. This memory address is the same one used by the Talk² Bootloader in case a "Factory Reset Flow" is triggered.

It's 100% safe to replace the Factory Firmware with your own code, including the firmware image saved on the SPI Flash. This firmware is just installed by default for testing propose and to enable Remote Programming out-of-the-box.

**Note that if you wish to Erase the whole SPI Flash memory, you need first to disable the write-protection.*

### Factory Reset Flow ###
To trigger the "Factory Reset Flow" on the Talk² Whisper Node board, you must hold the BT1 (Button 1) and power the board. Wait until the LD1 (Blue Led) starts to blink quickly and release the button while the LD1 still blinking. The LD2 (Yellow Led) will blink quickly a few times indicating the MCU is being programmed, followed by a reset.

For all bootloader features and documentation, please refer to the Repo https://bitbucket.org/talk2/talk2boot.

### Factory Firmware Configuration ###
One way to access the firmware configuration is to simply connect it to a serial port at the speed of 115200kbp/s:

![WhisperNode_FactoryFirmware_01.png](https://bitbucket.org/talk2/whisper-node-avr-lora/raw/master/Documentation/WhisperNode_FactoryFirmware_01.png)

Another way to find the configuration is be access the "Factory Firmware" example code, part of the "T2WhisperNode" library.

#### Configuration Table ####

| Parameter    | 433MHz | 868MHz | 915MHz |
|--------------|--------|--------|--------|
| Frequency    | 434MHz | 868MHz | 916MHz |
| TX Power     | 13dbM  | 13dbM  | 13dbM  |

### Remote Programming ###
TBD: How to use the Factory Firmware to remote program the Whisper Node.